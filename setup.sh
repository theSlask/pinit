#!/bin/bash

make all > /dev/null

if [ "$EUID" -ne 0 ]; then
	sudo "$0" "$@"
	exit $?
fi

echo Installing pinit!
echo Linking makefiles...
mkdir -p /usr/share/prv/
ln -f -s $(pwd)/makefiles /usr/share/prv
ln -f -s $(pwd)/templates /usr/share/prv
echo Linking binaries...
ln -f -s $(pwd)/bin/main /usr/bin/pinit
echo Setting up man pages...
install -g 0 -o 0 -m 0644 docs/pinit /usr/share/man/man1/pinit.1
gzip -f /usr/share/man/man1/pinit.1
sudo mandb -q
echo Done!

