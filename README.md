# Pinit

Pinit is an application that creates a new empty project for the supported
languages and project types. It can also be used to create files based on
templates. Currently the project contains Makefiles and Gitignore files for
C, C++, and Haskell. 

If you would like to contribute, please feel free to read the section about
contributing to the project.

## Installation

Pinit is boundled with a bash script for easier installation. Go to the folder
where the script is located and run it.

## How to Use

pinit [LANGUAGE] [FOLDER]

To create a project simply type "pinit" followed by the language code and a folder.
The folder is optional, and if a folder is ommited pinit will simply use the current
directory.

pinit [FILENAME] ...

To create a file based on a template, write "pinit" followed by the name of the file
with an extension. It will then try to find a template with the given extension.
You are able to write as many files you want


## Supported Projects and Languages

* C
* C++
* Haskell

## Supported File Templates

* C File [.c]
* C++ File [.cpp]
* C/C++ Header [.h]

## Long Time Support

The original bash version is still available as a long time support version.
It is possible to pull this from the PinitLTS branch. This will only be
updated with bugfixes. 

## Pinit Template Language

All the templates are fully customiceable, and it contains a lot of keywords which
will be evaluated and changed out upon creation. All these keywords are defined
with a '$' before and after. All keywords have a uppercase and lowercase version.
These can be accessed by writing a 'U' or 'L' before the keyword like this: 
$UBASENAME$.

* BASENAME      - The basename of the file.
* EXTENSION		- The extension of the file
* FILENAME		- The entire filename.
* PARENTDIR		- Parent directory to current.
* AUTHOR		- The author of the file
* DATE 			- Inserts the current date

## Pull Requests and Issues

I am open for any issues and pull requests. If you want to help out by creating
a general purpose Makefile, please make sure it follows the structure as the
other languages. If you make a pull requests which is approved, you will be mentioned
under credits.

## Contributing

Feel free to contribute in any way you want to. Everything from bug-reporting, adding
support for new languages, or improve the documentation. Any merge requests should be
sent to the development branch, where they will be reviewed by any maintainer or
developer. We welcome any contributions, everything from code to documentation.

## Distrubution

You are free to modify and share the program as you see fit. I would really appriciate
if you would give back to the community by sending the changes back here.

## Credits

* prvInSpace
* siaInSpace
* rur7
