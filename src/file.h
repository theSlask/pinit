//created by prv on 10/12/2018

#ifndef FILE_H
#define FILE_H

#include "list.h"

typedef struct file_tag {
	char *basename;
	char *extension;
	char *fullname;
} file;

void create_files(node *files);
int check_if_file(char *argv);


#endif
