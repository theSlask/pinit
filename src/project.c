
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "pinit.h"

static void copy_file(char *file, char *folder, char *path);

void create_project(char *language, char *folder)
{
	if(strcmp(folder, ".") != 0){
		struct stat st;
		if(stat(folder, &st) == -1){
			int status = mkdir(folder, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
			if(status){
				printf("Failed to create the project folder. Error code %d.", status);
				return;
			}
		}

		char src_folder[strlen(folder) + 5];
		strcpy(src_folder, folder);
		strcat(src_folder, "/src");
		if(stat(src_folder, &st) == -1){
			int status = mkdir(src_folder, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
			if(status){
				printf("Failed to create the source folder. Error code %d.", status);
				return;
			}
		}
	}

	char path[strlen(language) + strlen(MAKEFILES) + 2];
	strcpy(path, MAKEFILES);
	strcat(path, language);
	strcat(path, "/");

	DIR *d;
	struct dirent *dir;
	d = opendir(path);
	if(d) {
		while((dir = readdir(d)) != NULL){
			if(dir->d_name[0] != '.'){
				copy_file(dir->d_name, folder, path);
			}
		}
		closedir(d);
	} else {
		printf("Failed to open folder %s\n", path);
	}
}


/*
 * Checks if the spesified language is a folder.
 * Returns 1 if it exist.
 */
int does_language_exist(char *language)
{
	char path[strlen(MAKEFILES) + strlen(language + 1)];
	strcpy(path, MAKEFILES);
	strcat(path, language);
	struct stat sb;
	if(stat(path, &sb) == 0 && S_ISDIR(sb.st_mode))
		return 1;
	return 0;
}

static void copy_file(char *file, char *folder, char *path)
{
	FILE *src, *dst;
	
	char src_path[strlen(file) + strlen(path) + 1];
	strcpy(src_path, path);
	strcat(src_path, file);
	
	char dst_path[strlen(file) + strlen(folder) + 3];
	strcpy(dst_path, folder);
	strcat(dst_path, "/");
	if(strcmp(file, "gitignore") == 0)
		strcat(dst_path, ".");
	strcat(dst_path, file);

	src = fopen(src_path, "r");
	dst = fopen(dst_path, "w");

	if(src && dst){
		char buffer[256];
		while(fgets(buffer, sizeof(buffer) / sizeof(buffer[0]), src)) {
			fprintf(dst, "%s", buffer);
		}
		fclose(src);
		fclose(dst);
	}
	else {
		if(!dst)
			printf("Failed to open %s!\n", dst_path);
		if(!src)
			printf("Failed to open %s!\n", src_path);
	}
}
