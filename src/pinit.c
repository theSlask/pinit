
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "file.h"
#include "list.h"
#include "project.h"

#define NAME "pinit"
#define VERSION "2.0.1"

static void check_arguments(int argc, char **argv);
static void print_help();
static void print_version();

int MODE = -1;

char *MAKEFILES = "/usr/share/prv/makefiles/";
char *TEMPLATES = "/usr/share/prv/templates/";
char *DEFAULT_PROJECT_FOLDER = ".";

char *LANGUAGE;
char *FOLDER;
node *FILES = NULL;


int main(int argc, char **argv)
{
	if(argc == 1){
		printf("No arguments provided! Please write pinit --help for more information.\n");
		exit(0);
	}

	check_arguments(argc, argv);

	if(MODE == 0)
		create_project(LANGUAGE, FOLDER);
	else if(MODE == 1) {
		create_files(FILES);
	}
	else
		printf("Unknown command!\n");

	return 0;
}


static void check_arguments(int argc, char **argv)
{
  for(int i = 1; i < argc; ++i){
	// Help prompt
	if(strcmp(argv[i], "--help") == 0){
		print_help();
		exit(0);
	}

	// Version prompt
	else if(strcmp(argv[i], "--version") == 0){
		print_version();
		exit(0);
	}

	// Check if arg is a known language
	else if(does_language_exist(argv[i]) && MODE == -1){
		MODE = 0;
		LANGUAGE = argv[i];
		if(i + 1 != argc) {
			FOLDER = argv[i+1];
			i++;
		} else {
			FOLDER = DEFAULT_PROJECT_FOLDER;
		}
	}

	// Check if arg is a filename
	else if(check_if_file(argv[i]) && (MODE == -1 || MODE == 1)){
		MODE = 1;
	}

	// Unknown argument, exits the program.
	else {
		printf("Unknown argumment %s\n", argv[i]);
		exit(1);
	}
  }
}


/*
 * Prints the help message
 */
static void print_help()
{
	char *help[] = {
		"                         "NAME" "VERSION"\n",
		"A program which creates projects or file for any given programming",
		"lanuguage. For more information regarding which languages and",
		"extensions are available, please refer to the README.md file.\n",
		"Create a project: pinit [language] [OPTIONAL: Folder]",
		"Create a file: pinit [Filename]...\n",
		"Additional Arguments:\n"
		"--help",
		"   Displays this message and exits.\n",
		"--version",
		"   Displays the name and version of the program"
	};
	for(int i = 0; i < sizeof(help) / sizeof(help[0]); ++i)
		printf("%s\n", help[i]);
}

static void print_version()
{
	char *text[] = {
		NAME " " VERSION
	};
	for(int i = 0; i < sizeof(text) / sizeof(text[0]); ++i)
		printf("%s\n", text[i]);
}



