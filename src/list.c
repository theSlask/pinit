//created by prv on 10/12/2018

#include <stdio.h>
#include <stdlib.h>

#include "list.h"

static node* create_node(void *element);

void add_element(node **head, void *element){
	node *n = create_node(element);
	if(*head)
		n->next = *head;
	*head = n;
}

static node* create_node(void *element)
{
	node *n = malloc(sizeof(node));
	n->next = NULL;
	n->data = element;
	return n;
}

