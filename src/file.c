//created by prv on 10/12/2018

#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include "file.h"
#include "list.h"
#include "pinit.h"

static void create_file(file *f);
static void process_template(FILE *template, file *fl);
static int check_for_word(file *fl, FILE *output, char* word);
static void fprint_upper(FILE *output, char *c);
static void fprint_lower(FILE *output, char *c);


void create_files(node *files)
{
	while(files) {
		create_file(files->data);
		files = files->next;
	}
}

static void create_file(file *fl)
{
	char template_path_length = strlen(TEMPLATES) + strlen(fl->extension);
	char template_path[template_path_length + 1];
	char *template_name = "/template";
	char *link_name = "/link";
	
	char template_file_path[strlen(template_name) + template_path_length + 1]; 
	char link_file_path[strlen(link_name) + template_path_length];

	strcpy(template_path, TEMPLATES);
	strcat(template_path, fl->extension);
	
	strcpy(template_file_path, template_path);
	strcat(template_file_path, template_name);

	strcpy(link_file_path, template_path);
	strcat(link_file_path, link_name);

	FILE *template = fopen(template_file_path, "r");
	if(!template){
		printf("Warning: No template file found for extension: %s\n", fl->extension);
	} else {
		process_template(template, fl);
	}

	fclose(template);

	FILE *link = fopen(link_file_path, "r");
	if(!link){
		printf("Info: No link file found for extension: %s\n", fl->extension);
	} else {
		char buffer[11];
		while(fscanf(link, "%10s", buffer) != EOF){
			printf("Do you want to create %s file for %s? [y/N]: ", buffer, fl->fullname);
			char answer[11];
			fgets(answer, 10, stdin);
			if(answer[0] == 'y' || answer[0] == 'Y'){
				file newfile;
				newfile.basename = fl->basename;
				newfile.extension = buffer;
				char fullname[strlen(newfile.basename) + strlen(buffer) + 2];
				strcpy(fullname, newfile.basename);
				strcat(fullname, ".");
				strcat(fullname, buffer);
				newfile.fullname = fullname;

				//Open the right template file
				char new_template_file_path[strlen(TEMPLATES) + strlen(buffer) + strlen(template_name) + 2];
				strcpy(new_template_file_path, TEMPLATES);
				strcat(new_template_file_path, buffer);
				strcat(new_template_file_path, "/");
				strcat(new_template_file_path, template_name);
				FILE *new_template = fopen(new_template_file_path, "r");
				if(!template){
					printf("Warning: No template file found for extension: %s\n", buffer);
				} else {
					process_template(new_template, &newfile);
				}
				fclose(new_template);
			}
		}
	}
}

static void process_template(FILE *template, file *fl)
{
	char current;
	int BUFFER_SIZE = 20;
	char buffer[BUFFER_SIZE];
	int length;
	char delim = '$';

	//Open output in readmode to check if it exist.
	FILE *output = fopen(fl->fullname, "r");
	if(output) {
		printf("%s already exist, do you want to override it? [y/N]: ", fl->fullname);
		fgets(buffer, 19, stdin);
		if(buffer[0] != 'y' && buffer[0] != 'Y'){
			fclose(output);
			return;
		}
	}

	//Open output in write mode
	output = fopen(fl->fullname, "w");
	if(!output){
		printf("FATAL: Did not manage to open outputstream!\n");
		exit(1);
	}

	while(fscanf(template, "%c", &current) != EOF){
		if(current == '$'){
			int searching = 1;
			length = 0;
			while(searching){
				int status = fscanf(template, "%c", &buffer[length]);
				if(status == EOF)
					break;
				char c = buffer[length];
				if(length == BUFFER_SIZE - 2){
					buffer[BUFFER_SIZE -1] = 0;
					fprintf(output, "%c%s", delim, buffer);
					searching = 0;
				}
				else {
					switch(c) {
						case '\0':
						case '\r':
						case '\n':
							buffer[length + 1] = 0;
							fprintf(output, "%c%s", delim, buffer);
							searching = 0;
							break;
						case '$':

							buffer[length] = 0;
							status = check_for_word(fl, output, buffer);
							if(!status){
								printf("Unknown word in template file for %s: %s\n", fl->extension, buffer);
							}
							searching = 0;
							break;
					}
				}
				length++;
			}	

		} else {
			fprintf(output, "%c", current);
		}		
	}
	
	fclose(output);
}


static int check_for_word(file *fl, FILE *output, char* word)
{
	if(!strcmp(word, "BASENAME"))
		fprintf(output, "%s", fl->basename);
	else if(!strcmp(word, "UBASENAME"))
		fprint_upper(output, fl->basename);
	else if(!strcmp(word, "LBASENAME"))
		fprint_lower(output, fl->basename);
	else if(!strcmp(word, "EXTENSION"))
		fprintf(output, "%s",fl->extension);
	else if(!strcmp(word, "UEXTENSION"))
		fprint_upper(output, fl->extension);
	else if(!strcmp(word, "LEXTENSION"))
		fprint_lower(output, fl->extension);
	else if(!strcmp(word, "FULLNAME"))
		fprintf(output, "%s",fl->fullname);
	else if(!strcmp(word, "UFULLNAME"))
		fprint_upper(output, fl->fullname);
	else if(!strcmp(word, "LFULLNAME"))
		fprint_lower(output, fl->fullname);
	else if(!strcmp(word, "NAME")){
		char *login = getlogin();
		if(login)
			fprintf(output, "%s", login);
		else
			printf("Warning: Unable to fetch username from OS\n");
	}
	else if(!strcmp(word, "DATE")){
		time_t t = time(NULL);
		struct tm tm = *gmtime(&t);	
		fprintf(output, "%02d/%02d/%04d", tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900);
	}
	else {
		return 0;
	}
	return 1;
}

static void fprint_upper(FILE *output, char *c)
{
	while(*c){
		fprintf(output, "%c", toupper(*c));
		c++;
	}
}

static void fprint_lower(FILE *output, char *c)
{
	while(*c){
		fprintf(output, "%c", tolower(*c));
		c++;
	}
}
/*
 * Returns 0 if it does not find the spesified
 * language.
 */

int check_if_file(char *argv)
{
	char *ext = strrchr(argv, '.');
	if(ext) {
		int ext_length = strlen(ext);
		char *e = malloc(ext_length * sizeof(char));
		for(int i = 0; i < ext_length; i++)
			e[i] = ext[i+1];

		struct stat sb;
		char folder_path[ext_length + strlen(TEMPLATES)];
		strcpy(folder_path, TEMPLATES);
		strcat(folder_path, e);
		if(stat(folder_path, &sb) == 0 && S_ISDIR(sb.st_mode)) {
			file *f = malloc(sizeof(file));
			f->extension = e;
			f->fullname = argv;
			f->basename = malloc(strlen(argv) - ext_length + 1);
			int i;
			for(i = 0; i < strlen(argv) - ext_length; i++)
				f->basename[i] = argv[i];
			f->basename[i] = 0;
			add_element(&FILES, f);	
		} else {
			printf("Error: Did not find template for extension: %s\n", e);
			free(e);
		}
		return 1;
	}	
	else {
		return 0;
	}
}







